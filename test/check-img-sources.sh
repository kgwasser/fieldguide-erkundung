#!/bin/sh

# Testet, ob es genau so viele Bildquellen wie Figures gibt.
# Das sollte noch verbessert werden, sodass nur innerhalb der figure Tags geschaut wird.

figs=$(grep "begin{figure" $1/*.tex | wc -l)
sources=$(grep "(Bild: " $1/*.tex | wc -l)

echo "Checking for figure sources ..."

if [ $figs -ne $sources ]
then
	echo "ERROR: There are $figs figures and $sources sources."
	exit 1
fi

exit 0
