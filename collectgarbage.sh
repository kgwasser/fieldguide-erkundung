#!/bin/bash
# This script removes all unnecessary LaTeX files.

rm *.aux *.log *.bbl *.blg *.idx *.tdo *.toc *.ilg *.ind *~ *.lof *.lot *.out
rm *.fdb_latexmk *.fls 
rm *.synctex.gz
rm *.pdf *.jpg
rm .DS_Store thumbs.db
