# Field Guide Erkundung

Kompetenzgruppe Wasser, Ingenieure ohne Grenzen e. V.

## Überblick

*Nur PDF Dateien sind offizielle Versionen und dürfen verwendet werden!*

Dies ist das Repository für Field Guides "Erkundung" der KG Wasser. Im `master` Branch befindet sich die offizielle Version, im `draft` Branch wird der Field Guide weiterentwickelt. Offizielle Releases werden getaggt mit `git tag -a vX.Y` (`X.Y` ist durch die Versionsnummer zu ersetzen) und mit `git push origin --tag` an GitLab gesendet. GitLab baut aus jedem Tag eine neue PDF Datei und stellt diese auf http://kgwasser.gitlab.io bereit.

## Aktuelle PDF Versionen:

* http://kgwasser.gitlab.io/fieldguide-erkundung/erkundung.pdf